class Dictionary
 attr_accessor :entries

 def initialize
   @entries = {}
 end

 def add(entry)
   entries.merge!(entry) if entry.class == Hash
   entries[entry] = nil if entry.class == String
 end

 def keywords
   @entries.keys.sort
 end

 def include?(keyword)
   included = false
   @entries.keys.each do |hash_key|
     included = true if hash_key == keyword
   end
   included
 end

 def find(substr)
   entries.select do |k, v|
     k.match(substr)
   end
 end

def printable
  entries = keywords.map do |keyword|
    %Q{[#{keyword}] "#{@entries[keyword]}"}
  end
  entries.join("\n")
end



end
