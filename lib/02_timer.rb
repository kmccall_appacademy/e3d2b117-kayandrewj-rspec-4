class Timer
  attr_accessor :seconds
  def initialize(seconds=0)
    @seconds = seconds
  end
  def helper(number)
    if number > 9
      "#{number}"
    elsif number > 0
      "0#{number}"
    else
      "00"
    end
  end
  def hours
    Integer(seconds / 3600)
  end

  def minutes
    Integer((seconds % 3600) / 60)
  end

  def timer_seconds
    Integer((seconds % 60))
  end

  def time_string
    "#{helper(hours)}:#{helper(minutes)}:#{helper(timer_seconds)}"
  end
end
