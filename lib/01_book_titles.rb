class Book
  attr_reader :title
  def title=(title)
    title_cased = []
    stopwords = ["The", "A", "An", "And", "In", "Of"]
    title = title.split(" ").map(&:capitalize)
    title.each do |word|
      if stopwords.include?(word)
        title_cased << word.downcase
      else
        title_cased << word
      end
    end
    title_cased[0].capitalize!
    @title = title_cased.join(" ")
  end
end
