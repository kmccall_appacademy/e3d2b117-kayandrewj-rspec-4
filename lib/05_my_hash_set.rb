class MyHashSet
    attr_accessor :store

    def initialize
      @store = {}
    end

    def insert(el)
      store[el] = true
    end

    def include?(el)
      store.has_key?(el)
    end

    def delete(el)
      return false unless store.include?(el)
      store.delete(el)
      true
    end

    def to_a
      store.keys
    end

    def union(set)
      new_set = MyHashSet.new
      self.to_a.each do |el|
        new_set.insert(el)
      end
      set.to_a.each do |el|
        new_set.insert(el)
      end
      new_set
    end

    def intersect(set)
      new_set = MyHashSet.new
      self.to_a.each do |el|
        next unless set.include?(el)
        new_set.insert(el)
      end
      set.to_a.each do |el|
        next unless self.include?(el)
        new_set.insert(el)
      end
      new_set
    end

    def minus(set)
      new_set = MyHashSet.new
      self.to_a.each do |el|
        next if set.include?(el)
        new_set.insert(el)
      end
      new_set
    end

    def symmetric_difference(set)
      new_set = MyHashSet.new
      self.to_a.each do |el|
        next if set.include?(el)
        new_set.insert(el)
      end
      set.to_a.each do |el|
        next if self.include?(el)
        new_set.insert(el)
      end
      new_set
    end

end


# - Write a `set1#==(object)` method. It should return true if `object` is
#   a `MyHashSet`, has the same size as `set1`, and every member of
#   `object` is a member of `set1`.
